ARG BASE_IMAGE=elasticsearch:7.3.2

FROM $BASE_IMAGE

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu && \
    /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-phonetic