# elastic-analysis-icu-phonetic

Custom ElasticSearch Docker images containing the `analysis-icu` and `analysis-phonetic` plugins. 

## Images

The following images are available:

| Tags | Image | 
| ---- | -- | 
| `7.17.1` | `registry.gitlab.com/roemer/elastic-analysis-icu-phonetic:7.17.1` | 
| `8.5.2` | `registry.gitlab.com/roemer/elastic-analysis-icu-phonetic:8.5.2` | 

The images can be found on `registry.gitlab.com/roemer/elastic-analysis-icu-phonetic`; see https://gitlab.com/roemer/elastic-analysis-icu-phonetic/container_registry

The images are rebuild regularly to include upstream changes to the base images. 

## Getting started

The following `docker-compose` is an example of how to use this image for local development. 

1. Create a `docker-compose.yml` containing this image as a starting point:
    
    ```yaml
    version: '2'
    services:
      elasticsearch:
        image: registry.gitlab.com/roemer/elastic-analysis-icu-phonetic:7.3
        environment:
          - "discovery.type=single-node"
          - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
          - "cluster.routing.allocation.disk.threshold_enabled=false"
        ports:
          - 9200:9200
          - 9300:9300
      kibana:
        image: docker.elastic.co/kibana/kibana:7.3.0
        environment:
          SERVER_NAME: localhost
        ports:
          - 5601:5601
        depends_on: ['elasticsearch']
    ``` 

2. Start:

    ```bash
    docker-compose up 
    ```
 
  Elasticsearch is now reachable on http://localhost:9200. Kibana is accessible through http://localhost:5601.
  
