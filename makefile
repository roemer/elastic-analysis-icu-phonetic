BASE_IMAGE=elasticsearch
IMAGE_VERSION=7.7.1
IMAGE_NAME=new-elasticsearch


build:
	docker build --tag $(IMAGE_NAME):$(IMAGE_VERSION) --build-arg BASE_IMAGE=$(BASE_IMAGE):$(IMAGE_VERSION) .

deploy:
	docker push $(IMAGE_NAME):$(IMAGE_VERSION)
